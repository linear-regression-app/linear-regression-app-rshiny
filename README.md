# Linear Regression App - RShiny

A simple app for performing linear regression analysis / linear regression modelling within RShiny.

Features include data regression plots, variance inflation factors, colinearity matrices, model reporting and predction. All features are fully automated (through the use of reactive values), meaning that using the app requires no programming experience in R.

## Getting started

1. Make sure you have an up-to-date R Integrated Development Environment (IDE) 
    Reccommended: RStudio IDE https://www.rstudio.com/products/rstudio/
2. Pull this repository to your local machine and save the folder with a clear and informative name
3. Install the required packages for the script
4. Click run to load the packages into the environment
5. Run the RShiny linear regression modelling app

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files

```
cd existing_repo
git remote add origin https://gitlab.com/linear-regression-app/linear-regression-app-rshiny.git
git branch -M main
git push -uf origin main
```

## Visuals
**TODO Add Screenshots...**

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
**TODO Contact details to be added.**

## Roadmap
- September 2022: Add support for generating an RMarkdown report for only one variables
- December 2022: Add support for the .csv file type to accompany .xlsx support

## Contributing
Feedback or suggestions on the project are always welcome! **TODO Contact details to be added.**

## License
GNU General Public License v3.0

## Project status
Development may be limited due to work and study commitments.
